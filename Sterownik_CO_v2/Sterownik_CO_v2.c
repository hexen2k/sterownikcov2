/*
 * Sterownik_CO_v2.c
 *
 * Created: 2014-11-18 11:55:47
 *  Author: hexen2k mailto:hexen2k@gmail.com
 */ 

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include "uart.h"
#include "hd44780.h"
#include "onewire.h"
#include "ds18x20.h"

#define UART_BAUD_RATE 2400
#define MAXSENSORS	1
#define TIMEOWCONVERSION 75	//75 * 10 ms = 750 ms
#define TIMEOUTMENU 10	//10 s
#define RADIOINTERVAL 5	//5 s
#define STARTFRAME 0xF0

#define POMPAON()	PORTC|=1<<PC4
#define POMPAOFF()	PORTC&=~(1<<PC4)
#define RADIOON()	PORTB|=1<<PB5
#define RADIOOFF()	PORTB&=~(1<<PB5)
#define KEY_MENU_PRESSED	!(PINB&1<<PB0)
#define KEY_UP_PRESSED		!(PINB&1<<PB1)
#define KEY_DOWN_PRESSED	!(PINB&1<<PB2)
#define KEY_MENU	PB0
#define KEY_UP		PB1
#define KEY_DOWN	PB2

enum key {NUL, MENU, UP, DOWN, UPDOWN};

uint8_t gSensorIDs[MAXSENSORS][OW_ROMCODE_SIZE];
volatile uint8_t conversion_timer, timeout_menu, radio_timer;
volatile uint16_t pump_timer;
int16_t decicelsius;
uint8_t decicelsius_mod;
uint8_t ow_fresh_flag=0;	//TODO:: ow_fresh_flag will be used in future (trends on display)
uint8_t pump_level_switch=0, pump_safety_switch=0, pump_time_switch=0;

typedef struct {	
	uint8_t threshold;
	uint8_t hysteresis;
	uint8_t timeon;
	uint8_t timeoff;
	uint8_t protection_threshold;
} var_t;

var_t eem_var EEMEM = {45,5,10,20,90};	//default values
var_t ram_var;

static uint8_t search_sensors(void);
static int8_t decicelsius_correct(int16_t val);
void process_temperature(void);
enum key read_key(void);
void refresh_display(void);
void show_menu(void);
void process_output(void);
void send_radio_packet(void);

void Init(void){
	DDRC |= 1<<PC4;	//Output for LED and relay
	PORTB |= 1<<KEY_MENU | 1<<KEY_UP | 1<<KEY_DOWN;	//pull-up for keys
	DDRB |= 1<<PB5;	//output for radio transmitter switch
	RADIOOFF();
	
	//pull-up unused pin
	PORTA |= 1<<PA7;
	PORTB |= 1<<PB3 | 1<<PB4 | 1<<PB6 | 1<<PB7;
	PORTC |= 1<<PC0 | 1<<PC1 | 1<<PC2 | 1<<PC5 | 1<<PC6 | 1<<PC7;
	PORTD = 0xFF;	//all
		
	ACSR |= 1<<ACD;	//power off unused Analog Comparator
			
	lcd_init();
	LCD_DISPLAY(LCDDISPLAY);
	LCD_CLEAR;
	
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU));
	UCSRB |= 1<<TXCIE; //activate USART TXC interrupt for radio transmitter
	
	TCCR1B |= 1<<WGM12 | 1<<CS10 | 1<<CS12;	//Timer1 CTC mode, prescaler = 1024
	OCR1A = 15624;	//interrupt every 1 s
	
	TCCR2 |= 1<<WGM21 | 1<<CS22 | 1<<CS21 | 1<<CS20;	//CTC mode, prescaler = 1024
	OCR2 = 156;	//interrupt every 10 ms

	TIMSK |= 1<<OCIE1A | 1<<OCIE2;	//enable Timer1 and Timer2 interrupts
	
	eeprom_read_block(&ram_var, &eem_var, sizeof(ram_var));	//read values stored in eeprom memory	
}

int main(void){
	Init();
	
	LCD_LOCATE(0,0);
	lcd_puts_p(PSTR("Sterownik CO v2"));
	LCD_LOCATE(2,1);
	lcd_puts_p(PSTR("Michal Rajwa"));
	_delay_ms(2000);
	LCD_CLEAR;
	
	if (KEY_MENU_PRESSED){	//activate the continuous operation of the pump
		POMPAON();
		LCD_LOCATE(2,0);
		lcd_puts_p(PSTR("PRACA CIAGLA"));
		LCD_LOCATE(4,1);
		lcd_puts_p(PSTR("POMPY CO"));
		
		while(1)			
			;
	}
	
	wdt_enable(WDTO_2S);
	sei();
	
    while(1){			
		process_temperature();
		
		if(KEY_MENU_PRESSED) show_menu();		
		
		refresh_display();
		
		process_output();
		
		send_radio_packet();
		
		wdt_reset();
    }
}

void process_temperature(void){
	static uint8_t conversion_flag = 0;
	
	if(conversion_flag==0){
		if (search_sensors()){	//temperature sensor present
			if(DS18X20_start_meas(DS18X20_POWER_PARASITE,NULL) == DS18X20_OK){
				conversion_flag = 1;
				conversion_timer = TIMEOWCONVERSION;
			}
		}
		} else if (conversion_timer==0){	//after TIMEOWCONVERSION time (750 ms)
		if (DS18X20_read_decicelsius(&gSensorIDs[0][0], &decicelsius) == DS18X20_OK){
			ow_fresh_flag = 1;
			decicelsius_mod = decicelsius_correct(decicelsius);	//format data
		}
		conversion_flag = 0;
	}
}

enum key read_key(void){
	uint8_t tempKey = PINB;	// (PINB & (1<<KEY_MENU | 1<<KEY_UP | 1<<KEY_DOWN));
	enum key var_key = NUL;
	
	if(!(tempKey & 1<<KEY_UP) && !(tempKey & 1<<KEY_DOWN)) var_key = UPDOWN;
	else if(!(tempKey & 1<<KEY_UP)) var_key = UP;
	else if(!(tempKey & 1<<KEY_DOWN)) var_key = DOWN;	
	else if(!(tempKey & 1<<KEY_MENU)) var_key = MENU;
	
	_delay_ms(200);
	return var_key;
}

void refresh_display(void){
	uint16_t tmp_pump_timer;
	char buff[4];
	char buff2[5];
	char buff3[5];

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		tmp_pump_timer=pump_timer;
	}
		
	itoa((int)decicelsius_mod,buff2,10);
	itoa((int)tmp_pump_timer,buff3,10);
	
	LCD_LOCATE(0,0);
	lcd_puts_p(PSTR("Temp.   "));
	buff[0] = '\xDF';	//degree sign
	buff[1] = 'C';
	buff[2] = '\0';
	lcd_puts(buff);
	LCD_LOCATE(6,0);
	lcd_puts(buff2);
	
	LCD_LOCATE(0,1);
	lcd_puts_p(PSTR("Pompa "));	
	if(pump_safety_switch){
		lcd_puts_p(PSTR("ZAL.CIAGLE"));
		return;
	} else if(pump_level_switch && pump_time_switch) lcd_puts_p(PSTR("ZAL.       "));
	else lcd_puts_p(PSTR("WYL.       "));
		
	LCD_LOCATE(11,1);
	lcd_puts(buff3);
}

void process_output(void){
	uint16_t tmp_pump_timer;
	
	if(decicelsius_mod >= ram_var.threshold) pump_level_switch=1;	
	else if (decicelsius_mod < ram_var.threshold-ram_var.hysteresis) pump_level_switch=0;
		
	if(decicelsius_mod >= ram_var.protection_threshold) pump_safety_switch=1;
	else if (pump_safety_switch && (decicelsius_mod < ram_var.protection_threshold-ram_var.hysteresis)){
		pump_safety_switch=0;
		pump_time_switch=0;	//timeoff cycle after protection state
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
			pump_timer=ram_var.timeoff*60;
		}
	}
	
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		tmp_pump_timer=pump_timer;
	}
	
	if(pump_level_switch && !tmp_pump_timer){
		
		if(pump_time_switch==0){
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				pump_timer=ram_var.timeon*60;	//convert min to sec
			}	
			pump_time_switch=1;
		} else {
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
				pump_timer=ram_var.timeoff*60;
			}
			pump_time_switch=0;
		}				
	}	
		
	if(pump_safety_switch) POMPAON();
	else if(pump_level_switch && pump_time_switch) POMPAON();
	else POMPAOFF();	
}

void send_radio_packet(void){	
	if(radio_timer==0){
		RADIOON();	//power up radio transmitter (power down by ISR USART TXC - after transmission last sign)
		for (uint8_t i=0; i < 5; i++){
			uart_putc(0x00);
			uart_putc(0x00);
			uart_putc(0x00);
			uart_putc(STARTFRAME);
			uart_putc(decicelsius_mod);
			uart_putc(~decicelsius_mod);	//simple checksum
		}
		radio_timer=RADIOINTERVAL;
	}
}

void show_menu(void){
	char buff[4];
	enum key tmp_key;
	static uint8_t selection=1;
	
	LCD_CLEAR;
	timeout_menu = TIMEOUTMENU;
	LCD_LOCATE(0,0);
	lcd_puts_p(PSTR(" Z:   H:   B:"));
	LCD_LOCATE(0,1);
	lcd_puts_p(PSTR(" ON:    OFF:"));
	LCD_LOCATE(3,0);
	lcd_puts(itoa(ram_var.threshold,buff,10));
	LCD_LOCATE(8,0);
	lcd_puts(itoa(ram_var.hysteresis,buff,10));
	LCD_LOCATE(13,0);
	lcd_puts(itoa(ram_var.protection_threshold,buff,10));
	LCD_LOCATE(4,1);
	lcd_puts(itoa(ram_var.timeon,buff,10));
	LCD_LOCATE(12,1);
	lcd_puts(itoa(ram_var.timeoff,buff,10));
		
	_delay_ms(500);
	
	while ( ((tmp_key=read_key()) != MENU) && timeout_menu){
		if(tmp_key!=NUL) timeout_menu=TIMEOUTMENU;		
		if(tmp_key==UPDOWN){
			selection++;
			if(selection==6) selection=1;
		}
		
		switch (selection)
		{
		case 1:
			LCD_LOCATE(7,1);	//clear last selection sign ">"
			lcd_puts(" ");
			LCD_LOCATE(0,0);
			lcd_puts(">");
			if(tmp_key==UP && ram_var.threshold < 99) ram_var.threshold++;
			else if(tmp_key==DOWN && ram_var.threshold > 1) ram_var.threshold--;
			else break;
			LCD_LOCATE(3,0);
			lcd_puts("  ");
			LCD_LOCATE(3,0);
			lcd_puts(itoa(ram_var.threshold,buff,10));
			break;
		case 2:
			LCD_LOCATE(0,0);
			lcd_puts(" ");	
			LCD_LOCATE(5,0);
			lcd_puts(">");
			if(tmp_key==UP && ram_var.hysteresis < ram_var.threshold) ram_var.hysteresis++;
			else if(tmp_key==DOWN && ram_var.hysteresis > 1) ram_var.hysteresis--;
			else break;
			LCD_LOCATE(8,0);
			lcd_puts("  ");
			LCD_LOCATE(8,0);
			lcd_puts(itoa(ram_var.hysteresis,buff,10));
			break;
		case 3:
			LCD_LOCATE(5,0);
			lcd_puts(" ");
			LCD_LOCATE(10,0);
			lcd_puts(">");
			if(tmp_key==UP && ram_var.protection_threshold < 99) ram_var.protection_threshold++;
			else if(tmp_key==DOWN && ram_var.protection_threshold > (ram_var.threshold + ram_var.hysteresis)) ram_var.protection_threshold--;
			else break;
			LCD_LOCATE(13,0);
			lcd_puts("  ");
			LCD_LOCATE(13,0);
			lcd_puts(itoa(ram_var.protection_threshold,buff,10));
			break;
		case 4:
			LCD_LOCATE(10,0);
			lcd_puts(" ");
			LCD_LOCATE(0,1);
			lcd_puts(">");
			if(tmp_key==UP && ram_var.timeon < 99) ram_var.timeon++;
			else if(tmp_key==DOWN && ram_var.timeon > 1) ram_var.timeon--;
			else break;
			LCD_LOCATE(4,1);
			lcd_puts("  ");
			LCD_LOCATE(4,1);
			lcd_puts(itoa(ram_var.timeon,buff,10));
			break;
		case 5:
			LCD_LOCATE(0,1);
			lcd_puts(" ");
			LCD_LOCATE(7,1);
			lcd_puts(">");
			if(tmp_key==UP && ram_var.timeoff < 99) ram_var.timeoff++;
			else if(tmp_key==DOWN && ram_var.timeoff > 1) ram_var.timeoff--;
			else break;
			LCD_LOCATE(12,1);
			lcd_puts("  ");
			LCD_LOCATE(12,1);
			lcd_puts(itoa(ram_var.timeoff,buff,10));
			break;					
		default:
			break;	
		}
		wdt_reset();		
	}	
	
	eeprom_update_block(&ram_var, &eem_var, sizeof(ram_var));
	_delay_ms(500);
	LCD_CLEAR;
}

static uint8_t search_sensors(void)
{
	uint8_t i;
	uint8_t id[OW_ROMCODE_SIZE];
	uint8_t diff, nSensors;

	ow_reset();
	nSensors = 0;

	diff = OW_SEARCH_FIRST;
	while ( diff != OW_LAST_DEVICE && nSensors < MAXSENSORS ) {
		DS18X20_find_sensor( &diff, &id[0] );

		if( diff == OW_PRESENCE_ERR ||  diff == OW_DATA_ERR) break;

		for ( i=0; i < OW_ROMCODE_SIZE; i++ )
		gSensorIDs[nSensors][i] = id[i];

		nSensors++;
	}

	return nSensors;
}

static int8_t decicelsius_correct(int16_t val){
	int8_t temp;
	temp = val/10;
	if((val%10) >= 5) temp++;
	return temp;
}

ISR(TIMER1_COMPA_vect){	//every 1 s
	if(timeout_menu) timeout_menu--;	
	if(pump_timer) pump_timer--;
	if(radio_timer) radio_timer--;
}

ISR(TIMER2_COMP_vect){	//every 10 ms
	if (conversion_timer) conversion_timer--;

}

ISR(USART_TXC_vect){
	RADIOOFF();
}
